/*--------------------------------------------------------------------------+
 This file is part of ARSnova.
 internationalization.js
 - Beschreibung: Internationalisierung für ARSnova.
 - Version:      1.0, 01/05/12
 - Autor(en):    Christian Thomas Weber <christian.t.weber@gmail.com>
 +---------------------------------------------------------------------------+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or any later version.
 +---------------------------------------------------------------------------+
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 +--------------------------------------------------------------------------*/

// Android browser return always English as language, even when your phone is set up with a different one. Because of
// that, we read here the user agent and we extract from there the phone language. For all the other devices,
// "navigator.language" should give as the correct language.

var ua = navigator.userAgent.toLowerCase();
var isAndroid = ua.indexOf("android") > -1;
var lang;
if(isAndroid) {
    var lang_long = navigator.userAgent.match(/Android \d+(?:\.\d+){1,2}; [a-z]{2}-[a-z]{2}/).toString().match(/[a-z]{2}-[a-z]{2}/).toString();
    lang = lang_long.substring(0,2);
} else {
    lang = navigator.language;
}

var prefLang = localStorage.getItem("language");
if(prefLang != undefined){
	lang = prefLang;
}

switch (lang) {
	case 'en':case 'en-en':case 'en-us':
		Messages = {
			/* rolePanel */
			CHOOSE_ROLE: "Choose your role:",
			STUDENT: "Student",
			SPEAKER: "Teacher",
			/* loginPanel */
			CHOOSE_LOGIN: "Login as:",
			GUEST: "Guest",
			CHANGE_ROLE: "Change role",
			NO_GUEST_SPEAKER: "Note: You have to log in to create a session.",
			/* homePanel */
			LOGOUT: "Logout",
			LOGOUT_REQUEST: "Do you really want to logout?",
			SESSIONID_PLACEHOLDER: "Session-ID  (8-digits)",
			ENTER_SESSIONID: "Insert Session ID:",
			GO: "Go!",
			LAST_VISITED_SESSIONS: "Sessions you recently visited:",
			LEGEND: "Legend",
			OPEN_SESSION: "Open Session",
			CLOSED_SESSION: "Closed Session",
			
			/* LOAD MASK */
			LOAD_MASK: "Loading...",
			LOAD_MASK_LOGIN: "Login...",
			LOAD_MASK_SEARCH: "Looking for sessions...",
			LOAD_MASK_SEARCH_QUESTIONS: "Looking for questions...",
			LOAD_MASK_SEARCH_COURSES: "Looking for courses...",
			LOAD_MASK_SESSION_DELETE: "Deleting session data...",
			LOAD_MASK_ACTIVATION: "Releasing question...",
			
			/* mySessionsPanel */
			HOME: "Home",
			SESSION: "Session",
			CREATE_NEW_SESSION: "Create new session",
			NEW_SESSIONS: "New session",
			MY_SESSIONS: "My sessions",
			SESSIONS: "Sessions",
			SESSIONID_WILL_BE_CREATED: "Session ID will be created...",
			SESSION_NAME: "Name",
			SESSION_NAME_PLACEHOLDER: "max. 50 digits",
			SESSION_SHORT_NAME: "Short name",
			SESSION_SHORT_NAME_PLACEHOLDER: "max. 8 digits",
			SAVE: 'Save',
			
			/* canteen */
			CANTEEN: 'Canteen',
			I_RECOMMEND: "I recommend...",
			LOGIN: "Login",
			CANTEEN_MENU: "Menu",
			
			/* feedback */
			FEEDBACK: "Feedback",
			MY_FEEDBACK: "My feedback",
			FEEDBACK_GOOD: "Faster, please!",
			FEEDBACK_OKAY: "I can follow you.",
			FEEDBACK_BAD: "Slower, please!",
			FEEDBACK_NONE: "You've lost me.",
			FEEDBACK_INSTRUCTION: "Your feedback will be reset after ten minutes.<br><br>You can always give a new feedback.",
			FEEDBACK_VOTE: "vote",
			FEEDBACK_RESET: 'Your feedback has been reset',
			QUESTION_REQUEST: "I've got a question!",
			QUESTION_TO_SPEAKER: 'Question',
			QUESTION_INSTRUCTION: 'You are asking this question anonymously.<br>Your teacher decides when it will be answered.',
			QUESTION_TEXT: "Text",
			QUESTION_TEXT_PLACEHOLDER: 'max. 2500 characters',
			QUESTION_SUBJECT: "Subject",
			QUESTION_SUBJECT_PLACEHOLDER: "max. 140 characters",
			QUESTION_SAVED: 'Your question has been saved.',
			NOTIFICATION: "Notice!",
			TRANSMISSION_ERROR: "The question's transmission was unsuccessful.",
			QUESTION_CREATION_ERROR: "Failed creating the question.",
			ANSWER_CREATION_ERROR: "Your answer could not be saved.",
			
			/* questions */
			QUESTION: "Question",
			QUESTION_PLACEHOLDER: "Enter question",
			QUESTIONS: "Questions",
			QUESTION_DETAILS: "Details",
			QUESTION_DATE: "Details",
			DELETE: "Delete",
			EDIT: "Edit",
			CANCEL: "Cancel",
			DURATION: "Duration",
			NO_QUESTIONS: "No questions found.",
			NO_UNLOCKED_QUESTIONS: "There are prepared questions, but these are currently not unlocked by your teacher.",
			LOADING_NEW_QUESTIONS: "Loading new questions.",
			NO_DATE: "No date",
			NO_SUBJECT: "No subject",
			SESSION_CLOSE_NOTICE: 'In order to prevent multiple votings, your teacher has closed the session. Please log out at the end of the lecture!',
			NOTICE_READ: "I understand it",
			STATUS: "Status",
			FREETEXT_ANSWER_TEXT: "My Answer",
			FREETEXT_DETAIL_HEADER: "Answer",
			FREETEXT_DETAIL_ANSWER: "Answer",
			NO_ANSWERS: "No answers found.",
			ANSWER_SAVED: 'Your answer has been saved.',
			MISSING_INPUT: "Please fill out all required fields.",
			SHOWCASE: "Showcase",
			SHOWCASE_MODE: "Presentation Mode",
			LEAVE: "Leave",
			MEMBERS_ONLY: "This question is visible to course members only.",
			
			/* user */
			CHECK_ANSWERS: "Checking answers",
			QUESTIONS_TO_STUDENTS: "Teacher's questions",
			QUESTIONS_FROM_STUDENTS: "Students' questions",
			FLASHCARDS: "Flashcards",
			ONE_NEW_QUESTION: 'There is one new question.',
			WANNA_ANSWER: 'Would you like to answer now?',
			THERE_ARE: "There are",
			NEW_QUESTIONS: "new questions.",

			/* speaker */
			NEW_QUESTION: "New question",
			NEW_QUESTION_TITLE: "New question",
			AH_HOC_QUESTION: "Instant question",
			ARE_YOU_SURE: "Are you sure?",
			DELETE_SESSION_NOTICE: "All questions and answers of this session will be deleted.",
			DELETE_SESSION: "Delete session",
			CATEGORY: "Category",
			CATEGORY_PLACEHOLDER: "Enter category",
			ALL_SHORT: 'All',
			ONLY_THM_SHORT: 'THM only',
			ALL_LONG: 'All (incl. guest)',
			ONLY_THM_LONG: 'THM only',
			RELEASE_FOR: 'Release for:',
			ABSTENTION: 'Abstention',
			ABSTENTION_POSSIBLE: 'Abstention possible',
			MY_COURSES: "My courses:",
			COULD_NOT_SEARCH: "Could not search courses.",
			CORRECT_ANSWER: 'Correct answer',
			YES: "Yes",
			NO:	"No",
			NONE: "None",
			ANSWERS: "Answers",
			ANSWER: "Answer",
			COUNT: "Count",
			CORRECT: "Correct",
			CORRECT_PLACEHOLDER: "Correct answer",
			WRONG: "Wrong",
			WRONG_PLACEHOLDER: "Wrong answer",
			OPTION_PLACEHOLDER: "Option",
			STATISTIC: "Statistics",
			RELEASE_STATISTIC: "Release statistics",
			RELEASE_QUESTION: "Release question",
			STOP_QUESTION: "Stop question",
			START_SESSION: "Open session",
			STOP_SESSION: "Lock session",
			MARK_CORRECT_ANSWER: "Mark correct answer",
			DELETE_QUESTION: "Delete question",
			DELETE_ANSWERS: "Delete answers",
			DELETE_ANSWERS_REQUEST: "Delete answers?",
			QUESTION_REMAINS: "The question itself stays unaffected.",
			DELETE_ALL_ANSWERS_INFO: "This will also delete all previously given answers.",
			CHANGE_RELEASE: "Changing the release...",
			TYPE: 'Type',
			RELEASED: "Released",
			NOT_RELEASED: "Not released",
			INFINITE: "infinite",
			MINUTE: "Minute",
			MINUTES: "Minutes",
			BACK: "Back",
			
			/* question types */
			EVALUATION: "Eval",
			SCHOOL: "Grades",
			MC: "MC",
			YESNO: "Y/N",
			ABCD: "ABCD",
			FREETEXT: "Text",
			BUZZWORD_A: "Buzzword for A",
			BUZZWORD_B: "Buzzword for B",
			BUZZWORD_C: "Buzzword for C",
			BUZZWORD_D: "Buzzword for D",
			SCHOOL_A: "excellent",
			SCHOOL_B: "good",
			SCHOOL_C: "average",
			SCHOOL_D: "poor",
			SCHOOL_E: "very poor",
			SCHOOL_F: "inadequate",
			SCHOOL_NONE: "abstention",
			EVALUATION_PLUSPLUS: 	"strongly agree",
			EVALUATION_PLUS: 		"agree",
			EVALUATION_NEUTRAL: 	"don't know",
			EVALUATION_MINUS: 		"disagree",
			EVALUATION_MINUSMINUS: 	"strongly disagree",
			
			/* about */
			INFO: "Info",
			ABOUT: "About",
			ABOUT_ARSNOVA: "About ARSnova",
			ARS_IN_LECTURE: 'ARS in lecture',
			PREZI_ABOUT_ARS: 'Prezi about ARSnova',
			OPERATIONAL_AID: "Operational aid",
			WHAT_MEANS_ARS: 'What does \u201eARS\u201f mean?',
			ARS_IS_SOCIAL: 'ARSnova is \u201esocial software\u201f',
			DEVELOPMENT: "Development",
			CREDITS: "Credits",
			SPONSORS: "Sponsors of ARSnova",
			SPONSORS_SHORT: "Sponsors",
			OPENSOURCEPROJECTS: "Open Source Projects",
			OPENSOURCEPROJECTS_SHORT: "OSP",
			WIDGET_IS_LOADING: 'Widget is loading...',
			HELP: "Help",
			HELP_CANTEEN: "Help: Canteen",
			HELP_FEEDBACK: "Help: Feedback",
			HELP_HOME: "Help: Home",
			HELP_QUESTIONS: "Help: Questions",
			HELP_TO_HOME: "Help on Home",
			HELP_TO_FEEDBACK: "Help on Feedback",
			HELP_TO_QUESTIONS: "Help on Questions",
			HELP_TO_CANTEEN: "Help on Canteen",
			HELPDESK: "Helpdesk",
			IMPRESSUM: "Imprint",
			PRAISE_AND_CRITICISM: "Praise and criticism",
			SOCIAL_SOFTWARE: "Social software",
			OPEN_SESSIONS: "Sessions: open",
			ACTIVE_SESSIONS: "Sessions: active",
			CLOSED_SESSIONS: "Sessions: closed",
			NOT_RELEASED_YET: "The instructor has not yet released the correct answer.",
			STUDENTS_USE_CASES: "Student's Use Cases",
			TEACHERS_USE_CASES: "Teacher's Use Cases",
			LOG_IN_AND_GIVE_INSTANT_FEEDBACK: "Log in and give feedback",
			ASK_A_QUESTION: "Ask a question",
			ANSWER_A_QUESTION: "Answer a question",
			CREATE_A_SESSION: "Create a session",
			CREATE_A_QUESTION: "Pose a question",
			MANAGE_QUESTIONS_AND_ANSWERS: "Manage Q&A",

			/* credits */
			HMWK: "HMWK",
			ELEARNINGHESSEN: "Kompetenznetz E-Learning Hessen",
			AGQLS: "AG QLS",
			SENCHA_TOUCH: "UI Library: Sencha Touch",
			NGINX: "Webserver: nginx",
			CAS: "Single Sign-On: CAS",
			COUCHDB: "NoSQL Database: CouchDB",
			XEN: "Virtualization: Xen",
			DEBIAN: "Operating System: Debian",	

			/* misc */
			SUPPORTED_BROWSERES: "ARSnova is best-viewed in a WebKit browser, e.g. Apple Safari or Google Chrome!",
			NEW_VERSION_TITLE: "New Version",
			NEW_VERSION_AVAILABLE: "A new version of ARSnova is available. Do you want to update?",
			PRESENTER: "Presenter"
		};
	
		break;
	default:
		Messages = {
			/* rolePanel */
			CHOOSE_ROLE: "Wählen Sie Ihre Rolle:",
			STUDENT: "Zuhörer/in",
			SPEAKER: "Dozent/in",
			/* loginPanel */
			CHOOSE_LOGIN: 'Wählen Sie Ihren Zugang:',
			GUEST: "Gast",
			CHANGE_ROLE: "Rolle wechseln",
			NO_GUEST_SPEAKER: "Hinweis: Um eine Session anzulegen, müssen Sie sich anmelden.",
			/* homePanel */
			LOGOUT: "Abmelden",
			LOGOUT_REQUEST: "Möchten Sie sich wirklich von ARSnova abmelden?",
			SESSIONID_PLACEHOLDER: "Session-ID  (8-stellig)",
			ENTER_SESSIONID: "Bitte Session-ID eingeben",
			GO: "Los!",
			LAST_VISITED_SESSIONS: "Kürzlich besuchte Sessions",
			LEGEND: "Legende",
			OPEN_SESSION: "Offene Session",
			CLOSED_SESSION: "Gesperrte Session",
			
			/* LOAD MASK */
			LOAD_MASK: "Lade Daten...",
			LOAD_MASK_LOGIN: "Login...",
			LOAD_MASK_SEARCH: "Suche Sessions...",
			LOAD_MASK_SEARCH_QUESTIONS: "Suche Fragen...",
			LOAD_MASK_SEARCH_COURSES: "Suche Kurse...",
			LOAD_MASK_SESSION_DELETE: "Lösche Session-Daten...",
			LOAD_MASK_ACTIVATION: "Aktiviere die Freigabe...",
			
			/* mySessionsPanel */
			HOME: "Home",
			SESSION: "Session",
			CREATE_NEW_SESSION: "Neue Session anlegen",
			NEW_SESSIONS: "Neue Session",
			MY_SESSIONS: "Meine Sessions",
			SESSIONS: "Sessions",
			SESSIONID_WILL_BE_CREATED: "Session-ID wird erzeugt...",
			SESSION_NAME: "Name",
			SESSION_NAME_PLACEHOLDER: "max. 50 Zeichen",
			SESSION_SHORT_NAME: "Kürzel",
			SESSION_SHORT_NAME_PLACEHOLDER: "max. 8 Zeichen",
			SAVE: 'Speichern',
			
			/* canteen */
			CANTEEN: 'Mensa',
			I_RECOMMEND: "Ich empfehle...",
			LOGIN: "Login",
			CANTEEN_MENU: "Speiseplan",
			
			/* feedback */
			FEEDBACK: "Feedback",
			MY_FEEDBACK: "Mein Feedback",
			FEEDBACK_GOOD: "Bitte schneller",
			FEEDBACK_OKAY: "Kann folgen",
			FEEDBACK_BAD: "Zu schnell",
			FEEDBACK_NONE: "Abgehängt",
			FEEDBACK_INSTRUCTION: "Ihr Feedback wird nach zehn Minuten zurückgesetzt.<br><br>Sie können jederzeit ein neues Feedback geben.",
			FEEDBACK_VOTE: "Feedback geben",
			FEEDBACK_RESET: 'Ihr Feedback wurde zurückgesetzt',
			QUESTION_REQUEST: 'Ich habe eine Frage...',
			QUESTION_TO_SPEAKER: 'Frage an den Dozenten',
			QUESTION_INSTRUCTION: 'Sie stellen diese Frage anonym.<br>Der Dozent entscheidet, wann sie beantwortet wird.',
			QUESTION_TEXT: "Frage",
			QUESTION_TEXT_PLACEHOLDER: 'max. 2500 Zeichen',
			QUESTION_SUBJECT: "Betreff",
			QUESTION_SUBJECT_PLACEHOLDER: "max. 140 Zeichen",
			QUESTION_SAVED: 'Ihre Frage wurde gespeichert',
			NOTIFICATION: "Hinweis!",
			TRANSMISSION_ERROR: "Die Übermittlung der Frage war leider nicht erfolgreich",
			QUESTION_CREATION_ERROR: "Das Erstellen der Frage war leider nicht erfolgreich",
			ANSWER_CREATION_ERROR: "Die Antwort konnte nicht gespeichert werden",
			
			/* questions */
			QUESTION: "Frage",
			QUESTION_PLACEHOLDER: "Frage eingeben",
			QUESTIONS: "Fragen",
			QUESTION_DETAILS: "Details",
			QUESTION_DATE: "Details",
			DELETE: "Löschen",
			EDIT: "Bearbeiten",
			CANCEL: "Abbrechen",
			DURATION: "Dauer",
			NO_QUESTIONS: "Keine Fragen vorhanden.",
			NO_UNLOCKED_QUESTIONS: "Es gibt vorbereitete Fragen, diese sind aber noch nicht freigeschaltet.",
			LOADING_NEW_QUESTIONS: "Lade neue Fragen",
			NO_DATE: "Kein Datum",
			NO_SUBJECT: "Kein Betreff",
			SESSION_CLOSE_NOTICE: 'Um mehrfaches Abstimmen zu verhindern, hat Ihr Dozent die Session gesperrt. Loggen Sie sich bitte erst am Ende der Vorlesung aus!',
			NOTICE_READ: "Ich hab's verstanden",
			STATUS: "Status",
			FREETEXT_ANSWER_TEXT: "Meine Antwort",
			FREETEXT_DETAIL_HEADER: "Antwort",
			FREETEXT_DETAIL_ANSWER: "Antwort",
			NO_ANSWERS: "Keine Antworten vorhanden.",
			ANSWER_SAVED: 'Ihre Antwort wurde gespeichert.',
			MISSING_INPUT: "Es müssen alle Felder ausgefüllt werden.",
			SHOWCASE: "Vorstellen",
			SHOWCASE_MODE: "Präsentationsmodus",
			LEAVE: "Verlassen",
			MEMBERS_ONLY: "Diese Frage ist nur für Kursmitglieder sichtbar.",
			
			/* user */
			CHECK_ANSWERS: "Prüfe Antworten",
			QUESTIONS_TO_STUDENTS: "Fragen ans Publikum",
			QUESTIONS_FROM_STUDENTS: "Zwischenfragen",
			FLASHCARDS: "Lernkartei",
			ONE_NEW_QUESTION: 'Es gibt 1 neue Frage.',
			WANNA_ANSWER: 'Möchten Sie jetzt antworten?',
			THERE_ARE: "Es gibt",
			NEW_QUESTIONS: "neue Fragen.",
			
			/* speaker */
			NEW_QUESTION: "Neue Frage stellen",
			NEW_QUESTION_TITLE: "Neue Frage",
			AH_HOC_QUESTION: 'Sofort-Frage',
			ARE_YOU_SURE: "Sind Sie sicher?",
			DELETE_SESSION_NOTICE: "Es werden alle Fragen und Antworten der Session gelöscht.",
			DELETE_SESSION: "Session löschen",
			CATEGORY: "Kategorie",
			CATEGORY_PLACEHOLDER: "Kategorie eingeben",
			ALL_SHORT: 'Alle',
			ONLY_THM_SHORT: 'Nur THM',
			ALL_LONG: 'Alle (auch Gäste)',
			ONLY_THM_LONG: 'Nur THM-Mitglieder',
			RELEASE_FOR: 'Freigeben für:',
			ABSTENTION: 'Enthaltung',
			ABSTENTION_POSSIBLE: 'Enthaltung möglich',
			MY_COURSES: "Meine Kurse:",
			NO_COURSES_FOUND: "Es konnten keine Kurse gesucht werden.",
			CORRECT_ANSWER: 'Richtige Antwort',
			YES: "Ja",
			NO:	"Nein",
			NONE: "Keines",
			ANSWERS: "Antworten",
			ANSWER: "Antwort",
			COUNT: "Anzahl",
			CORRECT: "Richtig",
			CORRECT_PLACEHOLDER: "Richtige Antwort",
			WRONG: "Falsch",
			WRONG_PLACEHOLDER: "Falsche Antwort",
			OPTION_PLACEHOLDER: "Option",
			STATISTIC: "Statistik",
			RELEASE_STATISTIC: "Statistik freigeben",
			RELEASE_QUESTION: "Frage freigeben",
			STOP_QUESTION: "Frage sperren",
			START_SESSION: "Session freigeben",
			STOP_SESSION: "Session sperren",
			MARK_CORRECT_ANSWER: "Richtig markieren",
			DELETE_QUESTION: "Frage löschen",
			DELETE_ANSWERS: "Antworten löschen",
			DELETE_ANSWERS_REQUEST: "Antworten löschen?",
			QUESTION_REMAINS: "Die Frage bleibt erhalten.",
			DELETE_ALL_ANSWERS_INFO: "Es werden auch alle bisher gegebenen Antworten gelöscht.",
			CHANGE_RELEASE: "Ändere die Freigabe...",
			TYPE: 'Typ',
			RELEASED: "Freigegeben",
			NOT_RELEASED: "Nicht freigegeben",
			INFINITE: "unbegrenzt",
			MINUTE: "Minute",
			MINUTES: "Minuten",
			BACK: "Zurück",
			
			/* question types */
			EVALUATION: "Abfrage",
			SCHOOL: "Note",
			MC: "MC",
			YESNO: "J/N",
			ABCD: "ABCD",
			FREETEXT: "Text",
			BUZZWORD_A: "Schlagwort für A",
			BUZZWORD_B: "Schlagwort für B",
			BUZZWORD_C: "Schlagwort für C",
			BUZZWORD_D: "Schlagwort für D",
			SCHOOL_A: "sehr gut",
			SCHOOL_B: "gut",
			SCHOOL_C: "befriedigend",
			SCHOOL_D: "ausreichend",
			SCHOOL_E: "mangelhaft",
			SCHOOL_F: "ungenügend",
			SCHOOL_NONE: "Enthaltung",
			EVALUATION_PLUSPLUS: 	"trifft voll zu",
			EVALUATION_PLUS: 		"trifft eher zu",
			EVALUATION_NEUTRAL: 	"weiß nicht",
			EVALUATION_MINUS: 		"trifft eher nicht zu",
			EVALUATION_MINUSMINUS: 	"trifft nicht zu",
			
			/* about */
			INFO: "Info",
			ABOUT: "Über",
			ABOUT_ARSNOVA: "Über ARSnova",
			ARS_IN_LECTURE: 'ARS in der Lehre',
			PREZI_ABOUT_ARS: 'Prezi über ARSnova',
			OPERATIONAL_AID: "Bedienungshilfen",
			WHAT_MEANS_ARS: 'Was bedeutet \u201eARS\u201f?',
			ARS_IS_SOCIAL: 'ARSnova ist \u201eSocial Software\u201f',
			DEVELOPMENT: "Entwicklung",
			CREDITS: "Danksagung",
			SPONSORS: "Sponsoren von ARSnova",
			SPONSORS_SHORT: "Sponsoren",
			OPENSOURCEPROJECTS: "Open-Source-Projekte",
			OPENSOURCEPROJECTS_SHORT: "FOSS",
			WIDGET_IS_LOADING: 'Widget wird geladen...',
			HELP: "Hilfe",
			HELP_CANTEEN: "Hilfe: Mensa",
			HELP_FEEDBACK: "Hilfe: Feedback",
			HELP_HOME: "Hilfe: Startseite",
			HELP_QUESTIONS: "Hilfe: Fragen",
			HELP_TO_HOME: "Hilfe zu Home",
			HELP_TO_FEEDBACK: "Hilfe zu Feedback",
			HELP_TO_QUESTIONS: "Hilfe zu Fragen",
			HELP_TO_CANTEEN: "Hilfe zu Mensa",
			HELPDESK: "Helpdesk",
			IMPRESSUM: "Impressum",
			PRAISE_AND_CRITICISM: "Lob und Tadel",
			SOCIAL_SOFTWARE: "Social Software",
			OPEN_SESSIONS: "Sessions: offen",
			ACTIVE_SESSIONS: "Sessions: aktiv",
			CLOSED_SESSIONS: "Sessions: geschlossen",
			NOT_RELEASED_YET: "Der Dozent hat die richtige Antwort noch nicht freigegeben.",
			STUDENTS_USE_CASES: "Für Studenten",
			TEACHERS_USE_CASES: "Für Dozenten",
			LOG_IN_AND_GIVE_INSTANT_FEEDBACK: "Einloggen und abstimmen",
			ASK_A_QUESTION: "Eine Frage stellen",
			ANSWER_A_QUESTION: "Eine Frage beantworten",
			CREATE_A_SESSION: "Neue Sitzung erstellen",
			CREATE_A_QUESTION: "Neue Frage stellen",
			MANAGE_QUESTIONS_AND_ANSWERS: "Abfragen verwalten",

			/* credits */
			HMWK: "HMWK",
			ELEARNINGHESSEN: "Kompetenznetz E-Learning Hessen",
			AGQLS: "AG QLS",
			SENCHA_TOUCH: "UI Library: Sencha Touch",
			NGINX: "Webserver: nginx",
			CAS: "Single Sign-On: CAS",
			COUCHDB: "NoSQL Datenbank: CouchDB",
			XEN: "Virtualisierung: Xen",
			DEBIAN: "Betriebssystem: Debian",
			
			/* misc */
			SUPPORTED_BROWSERES: "Für eine korrekte Darstellung von ARSnova benutzen Sie bitte einen WebKit-Browser, z.B. Apple Safari oder Google Chrome!",
			NEW_VERSION_TITLE: "Neue Version",
			NEW_VERSION_AVAILABLE: "Eine neue Version von ARSnova ist verfügbar. Möchten Sie aktualisieren?",
			PRESENTER: "Presenter"
	};
	break;
}
